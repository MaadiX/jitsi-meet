class helpers::jitsi (
  Boolean $enabled  = str2bool($::jitsi_enabled),
  Boolean $constrainframerate = false,
  String $domain    = '',
) {

  validate_bool($enabled)

  if $enabled {

    #debconf script
    file { 'jitsi deconf script':
        path    => '/etc/maadix/scripts/jitsi-debconf.sh',
        owner   => 'root',
        group   => 'root',
        mode    => '700',
        content => template("helpers/jitsi-debconf.sh.erb"),
    }->
    #configure jitsi debconf only if it's not installed
    exec { 'configure jitsi debconf':
        command => '/etc/maadix/scripts/jitsi-debconf.sh',
        unless  => 'dpkg -l | grep jitsi',
        require => File['jitsi deconf script'],
        before  => Package['jitsi-meet'],
    } 

    ##add authorizedservice: jitsi if not present
    file { 'authorizedservice jitsi script':
      path    => '/etc/maadix/scripts/jitsi_authorizedservices.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/jitsi_authorizedservices.sh.erb'),
    } ->
    exec { 'add authorizedservice jitsi':
      command => "/bin/bash /etc/maadix/scripts/jitsi_authorizedservices.sh",
      creates => '/etc/maadix/status/jitsi_authorizedservices',
    }

    #reinstall if domain changes
    if($::jitsidomain_old){
      exec { 'purge jitsi':
        command => '/usr/bin/apt remove --purge -y jicofo jitsi-meet jitsi-meet-prosody jitsi-meet-web jitsi-meet-web-config jitsi-videobridge2 prosody prosody-modules lua-ldap',
        require => File['jitsi deconf script'],
      }->
      exec { 'configure jitsi debconf with new domain':
        command => '/etc/maadix/scripts/jitsi-debconf.sh',
      }->
      exec { 'purge prosody conf':
        command => '/bin/rm -r /etc/prosody',
        onlyif  => 'test -d /etc/prosody'
      }->
      exec { 'purge jitsi conf':
        command => '/bin/rm -r /etc/jitsi',
        onlyif  => 'test -d /etc/jitsi'
      }->
      exec { 'purge prosody var':
        command => '/bin/rm -r /var/lib/prosody',
        onlyif  => 'test -d /var/lib/prosody'
      }->
      exec { 'reinstall jitsi':
        command => '/usr/bin/apt install -y jitsi-meet',
      }->
      exec { 'reinstall prosody ldap support':
        command => '/usr/bin/apt install -y prosody-modules lua-ldap',
        before  => [
                   Package['jitsi-meet'],
                   Package['prosody-modules'],
                   Package['lua-ldap'],
                   ],
      }
    }


    #jitsi packages
    include apt

    exec {'jitsi apt key':
        command   => '/usr/bin/wget -q -O- https://download.jitsi.org/jitsi-key.gpg.key | /usr/bin/apt-key add -',
        unless    => 'apt-key list | grep "66A9 CD05 95D6 AFA2 4729  0D3B EF8B 479E 2DC1 389C"',
        logoutput => true,
    } ->
    apt::source { 'jitsi':
        location    => 'https://download.jitsi.org',
        repos       => '',
        release     => 'stable/',
    } ->
    package { 'jitsi-meet':
        ensure  => 'latest',
        require => Exec[apt_update],
        notify  => [
                   Service['jitsi-videobridge2'],
                   Service['prosody'],
                   Service['jicofo'],
                   ],
    }->
    package { 'prosody-modules':
        ensure  => 'present',
        require => Exec[apt_update],
    }->
    package { 'lua-ldap':
        ensure  => 'present',
        require => Exec[apt_update],
    }

    ### secrets

    #jicofo auth passwd
    exec { 'jicofoauth passw':
      command => "/bin/bash -c 'umask 077 && cat /etc/jitsi/jicofo/config | grep JICOFO_AUTH_PASSWORD | sed 's/JICOFO_AUTH_PASSWORD=//g' | tr -d \"\n\" > /etc/maadix/jicofoauth'",
      require => [
                 Package['jitsi-meet'],
                 Package['pwgen'],
                 File['/etc/maadix'],
                 ],
    }
    #jicofo passwd
    exec { 'jicofo passw':
      command => "/bin/bash -c 'umask 077 && cat /etc/jitsi/jicofo/config | grep JICOFO_SECRET | sed 's/JICOFO_SECRET=//g' | tr -d \"\n\" > /etc/maadix/jicofo'",
      require => [
                 Package['jitsi-meet'],
                 Package['pwgen'],
                 File['/etc/maadix'],
                 ],
    }
    #jitsivideobridge passwd
    exec { 'jitsivideobridge passw':
      command => "/bin/bash -c 'umask 077 && cat /etc/jitsi/videobridge/config | grep JVB_SECRET | sed 's/JVB_SECRET=//g' | tr -d \"\n\" > /etc/maadix/jitsivideobridge'",
      require => [
                 Package['jitsi-meet'],
                 Package['pwgen'],
                 File['/etc/maadix'],
                 ],
    }


    ### conf with secrets

    #jitsi-videobridge config template
    file { "jitsi-videobridge config_template":
        path    => "/etc/jitsi/videobridge/config_template",
        content => template('helpers/videobridge-config.erb'),
        require => [
                    Package['lua-ldap'],
                   ],
    }
    #jitsi-videobridge config with secrets based on template
    exec { "jitsi-videobridge config with secrets":
      command     => "/bin/bash -c 'cat /etc/maadix/jitsivideobridge | xargs -i sed \"s/jitsivideobridgePLACEHOLDER/{}/\" /etc/jitsi/videobridge/config_template > /etc/jitsi/videobridge/config'",
      umask       => '077',
      require     => [
                     File["jitsi-videobridge config_template"],
                     ],
      subscribe   => [ 
                     File["jitsi-videobridge config_template"],
                     ],
      refreshonly => true,
      notify      => Service['jitsi-videobridge2'],
    }

    #jitsi videobridge domain config
    file { "jitsi videobridge $domain-config.js":
        path    => "/etc/jitsi/meet/$domain-config.js",
        content => template('helpers/domain-config.js.erb'),
        require => [
                    Package['lua-ldap'],
                   ],
        notify  => Service['jitsi-videobridge2'],
    }

    #jicofo sip-communicator.properties
    file { "jicofo sip-communicator.properties":
        path    => "/etc/jitsi/jicofo/sip-communicator.properties",
        content => template('helpers/sip-communicator.properties.erb'),
        require => [
                    Package['lua-ldap'],
                   ],
        notify  => Service['jicofo'],
    }

    #jicofo config template
    file { "jicofo config_template":
        path    => "/etc/jitsi/jicofo/config_template",
        content => template('helpers/jicofo-config.erb'),
        require => [
                    Package['lua-ldap'],
                   ],
    }
    #jicofo config with secrets FIRST based on template
    exec { "jicofo config_template with secrets 1":
      command     => "/bin/bash -c 'cat /etc/maadix/jicofo | xargs -i sed \"s/jicofoPLACEHOLDER/{}/\" /etc/jitsi/jicofo/config_template > /etc/jitsi/jicofo/config'",
      umask       => '077',
      require     => [
                     File["jicofo config_template"],
                     ],
      subscribe   => [ 
                     File["jicofo config_template"],
                     ],
      refreshonly => true,
    }
    #jicofo config with secrets ANOTHER based on template with in place edition
    exec { "jicofo config_template with secrets 2":
      command     => "/bin/bash -c 'cat /etc/maadix/jicofoauth | xargs -i sed -i \"s/jicofoauthPLACEHOLDER/{}/\" /etc/jitsi/jicofo/config'",
      umask       => '077',
      require     => [
                     Exec["jicofo config_template with secrets 1"],
                     ],
      subscribe   => [ 
                     File["jicofo config_template"],
                     ],
      refreshonly => true,
      notify      => Service['jicofo'],
    }

    #prosody ldap settings template
    file { 'prosody ldap.cfg.lua_template':
        path    => '/etc/prosody/conf.avail/ldap.cfg.lua_template',
        content => template('helpers/ldap.cfg.lua_template.erb'),
        require => [
                    Package['lua-ldap'],
                   ],
    }
    #prosody ldap settings with secrets based on template
    exec { 'prosody ldap.cfg.lua with secrets':
      command     => "/bin/bash -c 'cat /etc/maadix/jitsildap | xargs -i sed \"s/jitsildapPLACEHOLDER/{}/\" /etc/prosody/conf.avail/ldap.cfg.lua_template > /etc/prosody/conf.d/ldap.cfg.lua && chown prosody /etc/prosody/conf.d/ldap.cfg.lua'",
      umask       => '077',
      require     => [
                     File['prosody ldap.cfg.lua_template'],
                     ],
      subscribe   => [ 
                     File['prosody ldap.cfg.lua_template'], 
                     ],
      refreshonly => true,
      notify      => Service['prosody'],
    }

    #prosody conf
    file { 'prosody prosody.cfg.lua':
        path    => '/etc/prosody/prosody.cfg.lua',
        content => template('helpers/prosody.cfg.lua.erb'),
        owner   => 'root',
        group   => 'prosody',
        mode    => '0640',
        require => [
                    Package['lua-ldap'],
                   ],
        notify  => Service['prosody'],
    }

    #prosody domain conf template
    file { "prosody $domain.cfg.lua_template":
        path    => "/etc/prosody/conf.avail/$domain.cfg.lua_template",
        content => template('helpers/domain.cfg.lua.erb'),
        require => [
                    Package['lua-ldap'],
                   ],
    }
    #prosody domain conf with secrets FIRST based on template
    exec { "prosody $domain.cfg.lua_template with secrets 1":
      command     => "/bin/bash -c 'cat /etc/maadix/jicofo | xargs -i sed \"s/jicofoPLACEHOLDER/{}/\" /etc/prosody/conf.avail/$domain.cfg.lua_template > /etc/prosody/conf.d/$domain.cfg.lua && chown prosody /etc/prosody/conf.d/$domain.cfg.lua && chmod 700 /etc/prosody/conf.d/$domain.cfg.lua'",
      umask       => '077',
      require     => [
                     File["prosody $domain.cfg.lua_template"],
                     ],
      subscribe   => [ 
                     File["prosody $domain.cfg.lua_template"],
                     ],
      refreshonly => true,
    }
    #prosody domain conf with secrets ANOTHER based on template with in place edition
    exec { "prosody $domain.cfg.lua_template with secrets 2":
      command     => "/bin/bash -c 'cat /etc/maadix/jitsivideobridge | xargs -i sed -i \"s/jitsivideobridgePLACEHOLDER/{}/\" /etc/prosody/conf.d/$domain.cfg.lua'",
      umask       => '077',
      require     => [
                     Exec["prosody $domain.cfg.lua_template with secrets 1"],
                     ],
      subscribe   => [ 
                     File["prosody $domain.cfg.lua_template"],
                     ],
      refreshonly => true,
      notify      => Service['prosody'],
    }


    #jitsi mods
    file { 'apache-jitsi':
        path    => '/etc/apache2/conf-available/jitsi.conf',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('helpers/jitsi.conf.erb'),
        require => Class['apache'],
    }
    file { '/etc/apache2/conf.d/jitsi.conf':
      ensure => 'link',
      target => '/etc/apache2/conf-available/jitsi.conf',
      notify => Service['httpd'],
      require => Class['apache'],
    }


    if($domain){
      #apache vhosts and certs
      apache::vhost{"$domain" :
        servername      => $domain,
        port            => 80,
        docroot         => "/var/www/jitsi",
        docroot_mode    => '0755',
        priority        => 90,
        override        => ['All'],
        rewrites   => [
          {
            comment        => 'redirect non-SSL traffic to SSL site but certbot .well-known folder',
            rewrite_cond   => ['%{REQUEST_URI} !^/\.well\-known/acme\-challenge/'],
            rewrite_rule   => ['(.*) https://%{HTTP_HOST}%{REQUEST_URI} [R=301,L]'],
          }
        ],
        notify => Class['apache::service'],
      } ->
      #remove vhost provided by package
      file{"/etc/apache2/sites-enabled/$domain.conf":
        ensure => 'absent',
      }->
      file{"/etc/apache2/sites-available/$domain.conf":
        ensure => 'absent',
      }->
      Exec['reload apache'] ->
      letsencrypt::certonly{$domain :
        domains   => [$domain],
        plugin    => 'webroot',
        webroot_paths => ["/var/www/jitsi"],
        require       => Package['python-ndg-httpsclient'],
      } ->
      apache::vhost{"$domain-ssl" :
        servername               => $domain,
        port                     => 443,
        docroot                  => "/usr/share/jitsi-meet",
        priority                 => 90,
        ssl                      => true,
        ssl_cert                 => "/etc/letsencrypt/live/$domain/cert.pem",
        ssl_chain                => "/etc/letsencrypt/live/$domain/chain.pem",
        ssl_key                  => "/etc/letsencrypt/live/$domain/privkey.pem",
        directories              => [
                                      { path              => '/usr/share/jitsi-meet',
                                        options           => ['FollowSymLinks','MultiViews', 'Includes', 'Indexes'],
                                        custom_fragment   => 'AddOutputFilter Includes html',
                                        allow_override    => 'All',
                                      },
                                    ],
        custom_fragment => "
  
  SSLProxyEngine on

  Header set Strict-Transport-Security 'max-age=31536000'

  ErrorDocument 404 /static/404.html

  Alias '/config.js' '/etc/jitsi/meet/$domain-config.js'
  <Location /config.js>
    Require all granted
  </Location>

  Alias '/external_api.js' '/usr/share/jitsi-meet/libs/external_api.min.js'
  <Location /external_api.js>
    Require all granted
  </Location>

  ProxyPreserveHost on
  ProxyPass /http-bind http://localhost:5280/http-bind/
  ProxyPassReverse /http-bind http://localhost:5280/http-bind/

  RewriteEngine on
  RewriteRule ^/([a-zA-Z0-9]+)$ /index.html

          ",
        notify => Class['apache::service'],
      }
    }
    
    #start services
    service { 'jitsi-videobridge2':
        ensure    => $enabled,
        name      => jitsi-videobridge2,
        enable    => $enabled,
        require   => Package['jitsi-meet'],
    }->
    service { 'prosody':
        ensure    => $enabled,
        name      => prosody,
        enable    => $enabled,
    }->
    service { 'jicofo':
        ensure    => $enabled,
        name      => jicofo,
        enable    => $enabled,
    }

    #clean old jitsi domain
    if($::jitsidomain_old){
      helpers::cleanvhost{"$::jitsidomain_old":
        docroot  => '/usr/share/jitsi-meet',
      }
    }


  } else {

    ##disable jitsi, prosody and jicofo
    service { 'jitsi-videobridge2':
        ensure    => $enabled,
        name      => jitsi-videobridge2,
        enable    => $enabled,
    }
    service { 'jicofo':
        ensure    => $enabled,
        name      => jicofo,
        enable    => $enabled,
    }
    service { 'prosody':
        ensure    => $enabled,
        name      => prosody,
        enable    => $enabled,
    }

    if($domain!=''){

      #remove apache vhosts
      apache::vhost{"$domain" :
        servername      => $domain,
        ensure          => 'absent',
        docroot         => "/var/www/jitsi",
        priority        => 90,
        port            => 80,
        notify          => Class['apache::service'],
      }
      apache::vhost{"$domain-ssl" :
        servername      => $domain,
        ensure          => 'absent',
        docroot         => "/var/www/jitsi",
        port            => 443,
        priority        => 90,
        notify          => Class['apache::service'],
        before          => Exec['reload apache'],
      }

    }


  }
}
