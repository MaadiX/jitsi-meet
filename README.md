Custom Puppet Module for Jitsi Meet installation in MaadiX
=============

This module installs Jitsi Meet with Ldap authentication with Puppet.
This is probably unuseful for others, as it has been developed taking into account the whole MaadiX system.  


# Description  
 
Installation is performed as canonical installation provided by Jitsimeet, which autogenerates secrets for Jicofo and JVB.
These secrets are locally stored to be  then used for creating new custom config files.  
This module does not include installation of other required packages, such as Apache, let's Encrypt...

# Features  

This Jitsi Meet installation includes following settings:  
 
* Force login to create a new room. Only authorized users are allowed to start a new room. Usres are stored in OpenLdap directory and need to have 'jitsi' as Authorized Srvice.  
It can be customizes in ldap.cfg.lua_template.erb  
* Option to change the installation domain 
* Video constrain set to 480 . See domain-config.js.erb 



# TODO  

* Use custom STUN server  
* Check other custom settings to increase room scalability  